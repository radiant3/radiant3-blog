
var theme = 'skeleton';

module.exports = {
    themeBase: 'hugo/themes/'+ theme + '/',
    themeStatic: 'hugo/themes/'+ theme + '/static/'
};