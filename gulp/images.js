var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var changed = require('gulp-changed');
var vars = require('./vars');

gulp.task('images', function () {
  return gulp.src([vars.themeStatic + 'images/**/*', 'hugo/static/images/**/*'])
    .pipe(changed('staging/images'))
    .pipe(imagemin())
    .pipe(gulp.dest('staging/images'));
});
