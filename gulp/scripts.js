var gulp = require('gulp');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var vars = require('./vars');

gulp.task('scripts', function() {
    return gulp.src(vars.themeStatic + 'js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter("default"))   
        .pipe(uglify())
        .pipe(gulp.dest('staging/js'));
});
