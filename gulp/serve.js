var gulp        = require("gulp");
var browserSync = require("browser-sync");
var vars        = require("./vars");


gulp.task('serve', ['build:all'], function() {
    // Serve files from the root of this project
    browserSync({
        server: {
            baseDir: "./public/"
        },
        open: false
    });

    // add browserSync.reload to the tasks array to make
    // all browsers reload after tasks are complete.
    gulp.watch([
        'hugo/layouts/**/*',
        'hugo/content/**/*',
        'hugo/archetypes/**/*',
        vars.themeBase + 'layouts/**/*',
        vars.themeBase + 'archetypes/**/*'
    ], ['build:content']);
    gulp.watch([
        'hugo/config.yaml',
        'hugo/static/**/*',
        vars.themeStatic + 'css/*.scss',
        vars.themeStatic + 'css/*.css',
        vars.themeStatic + 'js/*.js',
        vars.themeStatic + 'images/*.*'
    ], ['build:all']);
});
