var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var cleancss = require('gulp-clean-css');
var vars = require('./vars');

gulp.task('styles', function() {
    return gulp.src(vars.themeStatic + 'css/*.scss')
        .pipe(sass())
        .pipe(autoprefixer('last 2 version'))
        .pipe(cleancss({advanced:false}))
        .pipe(gulp.dest('staging/css'));
});
