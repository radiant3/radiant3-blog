#!/bin/bash

read -r -d '' USAGE << EOM
Builds this Hugo project and deploys resulting static files to an Amazon S3 bucket using s3cmd's sync operation.

  Usage: deploy_s3.sh [destination-s3-bucket]

Parameters:
    - destination-s3-bucket   The name of the S3 bucket where files are to be synced to.

Prerequisites:
    - The 's3cmd' command should be installed.
       >> https://github.com/s3tools/s3cmd/blob/master/INSTALL
    - A file named '.s3cfg' should define your S3 credentials to sync files to S3.
       >> http://s3tools.org/kb/item14.htm
    - The destination bucket must be setup to serve files as a website.
       >> http://docs.aws.amazon.com/AmazonS3/latest/dev/WebsiteHosting.html

Example:
    ./deploy_s3.sh blog.radiant3.ca
*********
EOM

BUILD_LOCATION=./public
S3_BUCKET=${1}

# safety checks
if [ -z "${S3_BUCKET}" ] ; then
    echo "${USAGE}"
    exit 1
fi

if [ ! -f '.s3cfg' ]; then
    echo "File '.s3cfg' does not exist. Please create it according to the instructions here: http://s3tools.org/kb/item14.htm"
    exit 1
fi

saferun() {
    "$@"
    local status=$?
    if [ $status -ne 0 ]; then
        echo "The command $1 ended with an error, aborting script." >&2
        exit 1
    fi
    return $status
}

echo "**** Cleaning up existing files..."
rm -rf ${BUILD_LOCATION}

echo "**** Building project "
saferun npm run gulp publish

echo "**** Uploading static files to S3..."
saferun s3cmd sync -c ./.s3cfg --delete-removed --add-header="Cache-Control:max-age=86400" ${BUILD_LOCATION}/ s3://${S3_BUCKET}

echo "**** Setting up redirects..."
# temporary workaround for keeping the RSS feed live at the old WordPress URL
saferun s3cmd put -c ./.s3cfg --add-header="x-amz-website-redirect-location: /index.xml" ${BUILD_LOCATION}/index.xml s3://${S3_BUCKET}/feed/index.html

echo "**** SUCCESS! Site deployed to S3 in bucket '${S3_BUCKET}'"
