radiant3-blog
=============

Sources for blog.radiant3.ca, started from [Hugulp](https://github.com/jbrodriguez/hugulp) starter.

The site is compiled using [Hugo](http://gohugo.io) and has a custom theme based on [Skeleton](http://www.getskeleton.com).

## Install Hugo

[Follow the instructions](http://gohugo.io/#action).

## Install Node

[Follow the instructions](https://nodejs.org)

If Node is already installed, no further action is required.

## Install other deps

 - Optipng `brew install optipng`

## (Optional) Install Pygments

If you plan on using syntax highlighting, Hugo's `highlight` shortcode requires the Pygments library to be available. It can be installed using Pip:

```
$ pip install Pygments
```

## Install NPM dependencies

After you cloned this repository locally, you need to install all dependencies:

```
$ npm install
```

This runs through all dependencies listed in `package.json` and downloads them to a `node_modules` folder in your project directory.

This will also install a copy of gulp locally (rather than globally), which is [generally preferable](http://jondavidjohn.com/keeping-it-local-with-npm-scripts/)

## Run gulp

Run the `default` gulp task with

```
$ npm run gulp
```

It will do the following:
- The **styles**, **scripts** and **images** tasks get executed first to do the heavy lifting of compressing images and minifying css/js files.
- The **revision** task runs next to fingerprint the optimized assets.
- Then the **hugo:all** task is invoked to generate the static site<br>
hugo will run as if invoked like this:
```
$ hugo --config=./hugo/config.yaml -s ./hugo -d ./public --buildDrafts=true --verbose=true --baseUrl="http://localhost:3000/"
```

- The **reference:all** task replaces all asset ocurrences with their fingerprinted versions
- Finally, the browser is reloaded so that you can very quickly check the changes you made

## Deploying to S3

```
$ ./deploy_s3.sh destination.s3.bucket
```

Before deployment, you need to install a few dependencies. See the deploy script's usage instructions for details by running:

```
$ ./deploy_s3.sh
```