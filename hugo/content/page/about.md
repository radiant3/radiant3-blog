---
type: page
title: About me
menu:
    main:
        name: "About"
        weight: 100
---

Hey there! My name is David Marquis and I'm a freelance full-stack software developer living in Montreal, Canada.

After coming back from a [long trip around the world in June 2016](http://www.twoseetheworld.com), I decided that I would try freelancing and founded Radiant3 inc., which is the company that I'm now operating my freelancing activities under.

This blog is the little spot on the Internet where I put some of my thoughts and a few tips I want to share with other software developers and leaders of software development teams. The older articles here have been carried over from my personal blog.

## Services

I'm fortunate enough to have a varied background (see [my story](/page/about/#my-story) below) in many areas of software development so I can help teams on many different levels. The services I'm focusing on for now include:

 - Full-Stack Open-Source Web Development
 - DevOps and Delivery Automation
 - Agile Practices and Coaching
 - Technical Team Leadership

Want to hire me? {{< button href="mailto:david@radiant3.ca" >}}Let's Get In Touch!{{< /button >}}

## Tools & Technologies

{{< row >}}
{{% column size="four" %}}
### Backend
- Java (Spring ecosystem, Hibernate, Maven, JUnit, Mockito, etc.), Kotlin
- Python (Django, REST Framework, Oscar Commerce, etc.)
{{< /column >}}
{{% column size="four" %}}
### Frontend
- HTML5, CSS/SASS/LESS
- ES6, Typescript
- React, Angular, Polymer, Riot, jQuery
- Mocha, Protractor, etc.
- Node-based builds (Grunt, Gulp, etc.)
{{< /column >}}
{{% column size="four" %}}
### DevOps
- Puppet
- AWS
- VMWare
- Docker
- Linux administration (CentOS/RHEL)
{{< /column >}}
{{< /row >}}
{{< row >}}
{{% column size="four" %}}
### Data
- PostgreSQL, MySQL, Oracle
- MongoDB, Redis
- RabbitMQ, ActiveMQ
- Elasticsearch, Logstash, Kibana
{{< /column >}}
{{% column size="four" %}}
### Automation & Monitoring
- Jenkins CI
- Fabric, Bash scripts
- Nagios, Graphite, Statsd
{{< /column >}}
{{< /row >}}

## My story

My love story with computers started more than 20 years ago, when I was around 10 years old. In the beginning, it was very simple stuff: trying (and succeeding) to circumvent the parental protection on my dad's computer, trying to configure a US Robotics 14.4k modem on Windows 3.11 (NOT easy at the time), playing with the DOS command line, fiddling with Basic. My dad saw my increasing interest for programming and bought me the complete version of Visual Basic 4.0. After an agonizing time installing it using the seemingly infinite number of floppy disks it came on, and reading the entire book accompanying it - a very large and heavy book - I started creating stuff.

The ability to make computers do whatever I wanted them to do felt very empowering and I already knew then that I was going to do that for a living.

### 1996 - First steps in entrepreneurship

Later on during high school I got a hold on Borland C++Builder and started learning C++. I really needed a concrete project to deepen my knowledge of the C++Builder platform so I convinced the school's sports center I was working at to create an application to track and bill for customers' time on the golf driving ranges. The app's main UI was composed of a tile of 18 panels, each one representing a single driving range and allowing the operator to start/stop/pause/resume timers. Each panel was tracking usage time as well as costs for each driving range. The app had its own internal database and needed to integrate with a thermal printer connected to the serial port for printing vouchers and recipes. I was 14 years old.

Eventually, I offered my services to the sports center's golf instructor to make him an application to manage his clients and courses schedule. The app was a lot more involved this time: it had an Access database and form-heavy user interface. It was awfully coded, and it was painful, but it was a tremendous learning opportunity.

I spent countless hours on those 2 projects, and in the end I was barely paid. But I guess it was a win/win situation: they were getting software for as cheap as cheap can be, and I was slowly but surely learning - the hard way - how to become a software developer.

### 1998 - Starting a web development business

As years passed by, I also got interested in design and visual stuff. I was able to get a hold on 3D Studio MAX and learned it enough to do some pretty cool 3D animations for projects at school. A friend who was also a computer geek suggested that we create a Quake II mod, which I happily accepted. I created and animated a custom sniper weapon for Quake II, which was awesome!

I eventually learned HTML, Photoshop and we started doing some websites. We each had our things but whenever we had opportunities for "client" projects we did them together.

In 1998 we launched a business to operate our website building and hosting activities. We were 16 years old and were proudly the youngest business owners to have opened an account at our local bank branch.

### 2003 - Diving in!

The years passed by as we were operating the business while still going to high school and college. We got a few significant web projects for local businesses and each of those projects really deepened our business and technical skills.
 
We had been working on a larger project with a French hotel owner operating in Cambodia. Not satisfied with the software options available on the market for managing his hotel, he wanted to build a custom solution and he found us on the Interwebs. The first iteration of that software was a good start and worked very well for them, but we had made so many mistakes building it that every change became very long and complex. We were drowning in technical debt and the only way out was to re-write it.

Thankfully, that same hotel owner had grand plans for his software: he wanted to build a commercial version that he could sell to other hotels. We partnered with him, he injected funds in our company and we started completely re-writing the software using more modern technologies. We learned Java, Swing, Derby (the Java embedded SQL database), XMPP, PHP, and countless other things.

It was a large project for two 20 years olds: the software had to handle everything from real-time online reservations to credit card payments, housekeeping, phone system (PBX) integration and even accounting/billing. Because it was meant to be a commercial product, it also had to be completely configurable and needed to be easily self-installable. We designed a innovative RPC protocol based on XMPP as a transport to allow bi-directional communications between the on-premise server component and our central on-line reservation system. The client application would send multicast packets to discover its server's IP address on the hotel's LAN so that no specific network configuration would be required during initial setup. We were very proud of what we had accomplished.

We called that software _Innfluence_ and it still has an odd-looking [website](http://www.innfluence.com/) with some less odd-looking [screenshots](http://www.innfluence.com/about/screenshots/) of the client Swing application!

### 2005 - A year in Cambodia

The hotel owner for whom we had created _Innfluence_ had a need for someone to manage his IT and accounting after the person who was working for him left. I decided it was too big an opportunity and I had to go. So in 2005, I moved to Cambodia.

At 22 years old, I suddenly had to manage a team of 13 cambodians, learn to manage a Windows Server and Active Directory network, handle payroll of the staff - personally handing the money to expats -, negotiate hardware purchases with locals, deploy a wifi network, handle systematic flooding in the office on every downpour during monsoon (yes, we were covering computers and servers with large plastic sheets every night), etc. etc.

I came back to Montreal after a year with countless memories and a newly found passion for travel.

### 2008 - Moving on...

After 10 years in business with my partner, multiple successful accomplishments and about as many failures, it was time for me to move on...

I took on a Java developer role in a large digital agency, participating in a large scale e-commerce project. With a growing interest on project management, I convinced my employer to give me a chance as project manager. I eventually came back to development when I subsequently got hired by a mobile messaging company.

In 2011 I got promoted to Technical Director in that same mobile messaging company. I led the technical team composed of 10 developers and ops people. It was a really amazing opportunity to develop my team building skills. Although it was a leadership role, I still found time to regularly keep my hands dirty, coding in pair with the guys, doing code reviews, automating the infrastructure and deployments, creating libraries, etc.

### 2015 - A pause to discover the world

After 4 years as Technical Director, my girlfriend and I left our jobs, sold the car, rented the house and jumped in the somewhat crazy project of completing a full revolution of the Earth. In the course of 8 months, we [visited 17 countries](http://www.twoseetheworld.com) (locations including Ecuador, Peru, Bolivia, Chile, New Zealand, Bali, Malaysia, Thailand, India, Vienna, Budapest, Prague, Spain, Morocco, Portugal, Netherlands and Iceland), traveling more than 63,000kms and accumulating more than 200 hours of bus on a distance of more than 4,000kms.

One of our projects during the trip has been to compile tens of photos of [us jumping in many places around the world](http://www.twoseetheworld.com/jumps/). We believe the end result is pretty cool! For the curious, I built that site using [Riot](http://riotjs.com/).

### 2016 and onwards - Radiant3 consulting

Immediately after our return from the rount-the-world trip, I started Radiant3 to offer my services in software development consulting, and I've been busy!

Want to hire me? {{< button href="mailto:david@radiant3.ca" >}}Let's Get In Touch!{{< /button >}}
