---
date:   2011-01-13T02:01:00Z
title:  Maven custom properties precedence
slug:   maven-custom-properties-precedence
url:    /2011/01/13/maven-custom-properties-precedence/
tags:
- java
- maven
---

If you've worked with Maven before, you are probably aware that it allows you to define custom properties that can then be used throughout your build as placeholders (for value re-use or configuration purposes) or even as a mean of configuring your final build resources using filters. These come very handy for defining environment-specific configurations.

These custom properties can be defined at multiple locations. But what happens when you define (or re-define) the same property in multiple locations? Which location will have precedence on others? Amazingly, even if Maven is very popular in the Java ecosystem, I could not find any useful reference on this.

After research and thorough testing, I've come up with this order of precedence which Maven uses when resolving custom properties:

  1. System properties: set with `-Dxyz=value` on the command line.
  2. From currently active profile(s): `settings.xml` in user home directory first, then `profiles.xml` in project root directory, then in profiles defined in your `pom.xml`.If many profiles are active, and a property is defined in more than one of those, the order of precedence is based on the last profile in which this property is defined, in alphabetical order of profile name.
  3. In the `properties` section of your `pom.xml`.
  4. Lastly, in properties defined in [filters](http://maven.apache.org/plugins/maven-resources-plugin/examples/filter.html). If a property is defined in multiple filters, then the last one (in order of appearance in your `filters` section) has precedence over the others.

Good to know!! This came handy recently for me so I share it here, hoping it will help someone else!
