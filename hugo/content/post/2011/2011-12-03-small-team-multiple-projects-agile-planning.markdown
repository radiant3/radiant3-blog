---
date:   2011-12-03T16:42:09Z
featured: true
title:  'Small team, multiple projects: an Agile approach to planning'
slug:   small-team-multiple-projects-agile-planning
url:    /2011/12/03/small-team-multiple-projects-agile-planning/
tags:
 - agile
 - management
aliases:
 - /2011/12/03/83745499/
---

Context: you have a small team of 5 people, evolving in a highly dynamic environment with small projects (2-4 weeks, sometimes less, sometimes more) coming all over the place for the team to realize. This is a very usual pattern observed in agencies, or smaller teams dedicated to professional services (services to clients).

How do you approach people planning (a.k.a "resource planning", although I won't hide my aversion for the term "resource" when referring to a person) in that kind of context? The inate approach to this problem seems to start assigning individual people from the team to different projects in the pipeline, trying to optimize utilization of people on an individual basis:

{{< figure src="/images/posts/teams-slide1.png" num="1" >}}
Sub-optimal organization in a team of 5 people individually scheduled.
{{< /figure >}}

On the diagram above, we have a fictive situation of 4 projects in the pipeline. The first 3 projects (A, B and C) are either already started or start at the beginning of week 1. The 4th project (D) starts on the 4th week.

Some observations:

  * Because we are assigning people as mere resources, suboptimal situations occur, such as people having nothing to do at specific times, for short period of times
  * Some people are clearly leads on some projects, while others are lugged between projects
  * Some people never participate in some projects, limiting knowledge to the few who worked on these projects
  * This is a quite simple situation, and it already looks a nightmare to manage. You quickly see the need for a new "traffic controller" job to manage and optimize people allocation. In a larger organization with more people, this quickly becomes a full-time job.
  * For project D, which starts on the fourth week, only 1 person is assigned. You'll need to be very careful for the rest of the project to plan for knowledge transfer: this person could become sick any day, leaving you in the dust.
  * From my experience, this kind of planning encourages individuality, leaving you not with a team, but with a bunch of individuals working separately. These individuals tend to not be aware or even sensible to what others are doing and their problems. They will also have a tendency to stop being proactive and _wait for job to be assigned to them_.

Far from ideal and not very agile! Unfortunately, it is a common situation.

I recently came up with a more systemic approach to the problem. Instead of assigning people to projects on an individual basis, why not see all projects as being _the work to be done_ and the team as being _the system which realizes the work to be done_?

Start seeing the group representing the team as a whole, capable of a certain capacity of work. Then, split this capacity in _value streams_. When planning for projects, assign projects to these value streams instead of the individuals.

{{< figure src="/images/posts/teams-slide2.png" num="2" >}}
Assigning projects to teams instead of individual people.
{{< /figure >}}

Before any project starts, make sure you have an estimated and prioritized backlog of features. At the beginning of each sprint, pull some work from these backlogs according to the planned projects in each value stream:

{{< figure src="/images/posts/teams-slide3.png" num="3" >}}
Pulling work from the projects backlog and assigning to a team.
{{< /figure >}}

You may need a project to be put on the fast lane for any reason (the project is late, or there's no other project in the pipeline). Then, simply assign more value streams to this project. This will allow more work for this project to be selected and included in the sprint:

{{< figure src="/images/posts/teams-slide4.png" num="4" >}}
Pulling more work from a specific project to accelerate it.
{{< /figure >}}

When a sprint starts, leave the team alone. If you are a manager, _trust_ the team to organize around the work to be done. Coach them to become more efficient in the way they work _together_. Always treat the team as a whole. Do not try to identify owners for projects yourself: they'll probably do it naturally. Encourage them to work in pairs for more complex problems. Make sure they take time to inspect how they work, then that they adapt.

When you start treating people as teams, some things start to happen naturally:

  * People start to care and worry for their team partners
  * Overall productivity increases
  * One person leaving (holidays, sickness) does not bring a project down anymore
  * People start actually enjoying their job a lot more

An important factor to the success of the approach is to only allow a number of value streams smaller than the number of individuals in the team. I would say a maximum of:

    floor(team size / 2) + 1

Examples:

  * Team of 3: 2 value streams
  * Team of 6: 4 value streams
  * Team of 7: 4 value streams

This has multiple effects:

  * Since there is less projects than people, this fosters collaboration, thus enabling natural knowledge transfer within the team.
  * It effectively creates a real team: a group of individuals working on the same goals together as a whole.
  * Suboptimal situations, like people having nothing to do, are removed.

This approach also has a potential for scaling to much more than a single team. If the number of projects is too high for a single team, create multiple teams, assign projects to teams, then plan these projects based on their value stream.

I've started this way of working about a month ago with the team I am leading, and they simply love it! Planning is also a lot simpler, allowing me (and others) to concentrate on other matters.

One thing that is clear is that the team bonding and productivity benefits do not happen overnight. As the [Virginia Satir Change Model](http://stevenmsmith.com/ar-satir-change-model/) states, one must accept reduced or steep variations in productivity on the first few weeks following the change, especially if the so-called "team" has been working individually for a while before the change.

If you try this approach (or have already tried it, or a variation of it), please leave some feedback! I'm very interested in knowing how this works out for you.
