---
date:   2011-01-05T03:11:00Z
title:  The leader and "his team"
slug:   the-leader-and-his-team
url: /2011/01/05/the-leader-and-his-team/
tags:
- agile
- management
---

People seem to have a hard time treating a group of individuals working on a common goal as a Team. I've observed that many people will naturally expect commitment and liability from leaders, even unofficially appointed leaders, or natural leaders, instead of teams.

But why is that so? Why are people seeing hierarchy in leadership?

The answer might come from us being deeply entangled in our [Fayolism](http://en.wikipedia.org/wiki/Henri_Fayol) roots. For most of the 19th and 20th centuries, management styles have always focused on creating a hierarchy of command and control. This has worked great in traditional, repetitive and manual work environments (which were prevalent up to a few decades ago), and this management style still sneaks in our daily lives as software professionals. Even with the best intentions to get away from these management styles, most people still experience a few behaviours reminiscent of these roots. The leader leads the team, thus is responsible for "his team". It looks like it is comforting to know that a single neck can be wrung if something happens.

It won't be a surprise to anyone that writing software is certainly not repetitive (most of it at least, if it _is_ for you, you should definitely consider giving your current job a good thought). A more collaborative, egalitarian work environment is often more appropriate, and has proven much healthier in many situations.

To help cultivate a more collaborative environment around me, I've come up with a few rules of thumbs that I try to honour continually as a team-focused leader:

  * Never, never commit on work on behalf of the team
  * Systematically use "My team and I" or "We" formulations when communicating those team commitments or decisions
  * Always estimate and plan in teams

Creating a team-focused mindset in daily communications with the different stakeholders will slowly but surely help in removing this wringable-leader delusion.
