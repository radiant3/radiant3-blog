---
date:   2011-06-23T02:10:00Z
title:  (Off topic) Plans for building a 8x12 shed - steps pictures included
slug:   plans-for-building-a-8x12-shed
aliases:
  - /2011/06/23/plans-for-building-a-8x12-shed-pictures-inclu/
---

Just completed today my shed building project for the backyard at our new house! This was an awesome project and I had a great time with my father and brother.

I thought I'd share some pictures and the rough plans for the project. I spent a great deal of time on the Web and into my local hardware store to find information related to the different steps of building the shed... Although I won't go into much details in this post, I hope the picture attached will help others get cues on how to do this and that.

{{< gallery >}}
{{< lightbox width="100" target="/images/posts/img_0132-scaled1000.jpg" >}}
{{< lightbox width="100" target="/images/posts/img_0134-scaled1000.jpg" >}}
{{< lightbox width="100" target="/images/posts/img_0135-scaled1000.jpg" >}}
{{< lightbox width="100" target="/images/posts/img_0136-scaled1000.jpg" >}}
{{< lightbox width="100" target="/images/posts/img_0137-scaled1000.jpg" >}}
{{< lightbox width="100" target="/images/posts/img_0143-scaled1000.jpg" >}}
{{< lightbox width="100" target="/images/posts/img_0142-scaled1000.jpg" >}}
{{< lightbox width="100" target="/images/posts/img_0145-scaled1000.jpg" >}}
{{< lightbox width="100" target="/images/posts/img_0146-scaled1000.jpg" >}}
{{< lightbox width="100" target="/images/posts/img_0147-scaled1000.jpg" >}}
{{< lightbox width="100" target="/images/posts/104-scaled1000.jpg" >}}
{{< lightbox width="100" target="/images/posts/img_0160-scaled1000.jpg" >}}
{{< lightbox width="100" target="/images/posts/img_0163-scaled1000.jpg" >}}
{{< lightbox width="100" target="/images/posts/img_0164-scaled1000.jpg" >}}
{{< /gallery >}}

The project took my father, my brother and me about 6 full days to complete (we're not experts!) but the end result is awesome. Some others facts:

  * I made the plans with [Google SketchUp](http://sketchup.google.com/)
  * Floor is held on 6 concrete blocks
  * Floor is built with 2x6s, spaced at 12 inches on-center
  * Plywood sheets were used to cover floor
  * Walls and roof are made up of 2x4s lumber, spaced 16 inches on-center
  * Aspenite sheets were used for covering walls (1/2 inch) and roof (5/8 inch)
  * Tar paper and fiberglass BP shingles were used for roof finition, tacked with 1 inch roofing nails
  * Window and door were pre-made (slide-ins)
  * Vinyl siding and soffits were used, as well as aluminium fascias. Aluminium was tacked into place using white aluminium nails (non corrosive)
  * An electric cable runs underground to the house and into the main panel to bring light and electricity into the shed

{{< lightbox width="200" target="/images/posts/cabanon_facade-scaled1000.png" >}}
{{< lightbox width="200" target="/images/posts/cabanon_side-scaled1000.png" >}}

Some random tips and afterthoughts:

  * Roof trusts can be made up-front (this is what we did), as long as dimensions are carefully thought out!
  * Use a pressure nailing gun with glued nails for carpentry works, this is fantastic and extremely fast
  * Use a pressure tacking gun for fixing aluminium and soffits
  * Building the shed with dimensions (width, depth and height) that are a multiple of 4 is convenient since most sheets are 4'x8'
  * Use aluminium trimming cisors to cut vinyl sheets. A handsaw works also but requires some help to hold the sheets while cutting.
  * We should have rented an aluminium folding machine for fascias: folding the fascias for sliding under the roof shingles was a real big pain.
  * We had to add some small pieces of lumber (3/4 x 3/4 inch) under the roof perimeter to allow for the aluminium fascia to be tacked. We should have used 1x4s on the extremities instead of 2x4s: this would have allowed the pre-shaped fascias fittings to correctly slide in and be tacked.
  * Finition is extremely time consuming it took 3.5 days to fully complete the structure and the roofing, then almost another 3 days to fully complete the shed (vinyl siding, soffits and fascias)

Hope this helps! If any question, please ask in the comments below!
