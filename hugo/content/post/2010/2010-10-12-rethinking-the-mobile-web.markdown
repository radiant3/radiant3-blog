---
date:   2010-10-12T02:08:00Z
title:  Rethinking the Mobile Web
slug:   rethinking-the-mobile-web
url:    /2010/10/12/rethinking-the-mobile-web/
tags:
- mobile
---

[See the Slideshare presentation](http://www.slideshare.net/bryanrieger/rethinking-the-mobile-web-by-yiibu)

Lots of insights on how we should start building web sites (and applications) for the mobile web, then progressively enhance for more capable devices (such as desktop computers).

Since mobile devices provide, by their intrinsic design and small form factor, a limited user experience, it makes a lot of sense to start thinking of mobile devices as our new smallest common denominator (our new IE6!), making our web sites work on mobile browsers first, then progressively enhance for fully capable desktop browsers (which also provide a richer user experience).

I'm in!
