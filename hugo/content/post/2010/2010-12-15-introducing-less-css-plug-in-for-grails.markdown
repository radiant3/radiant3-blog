---
date:   2010-12-15T00:15:00Z
title:  Introducing Less CSS plug-in for Grails
slug:   introducing-less-css-plug-in-for-grails
url:    /2010/12/15/introducing-less-css-plug-in-for-grails/
tags:
- frontend
- grails
---

[Less CSS](http://lesscss.org) is a CSS pre-processor that adds support for many advanced (and lifesaver) features to normal CSS, such as variables, mixins, nested definitions, etc, etc. You basically write .less files that have a syntax similar to CSS itself (with the added functionalities), and these files get compiled as CSS after on. I've been wanting to use it for a long time but support for Less in the Groovy/Java ecosystem is scarce. It has originally been developed by the Rails folks. The latest version of Less however has been re-written as pure Javascript. This opens up a full range of possibilities for integrating Less everywhere.

Grails developers, wait no more! I've just completed a first draft of an idea that I had this past summer after digging a little bit into the less.js implementation of Less, and this has finally taken the form of a [Grails Less plug-in](https://github.com/davidmarquis/grails-lesscss). The basic idea is to offer real-time compilation of Less files when developing your application, and statically compile your Less files as standard CSS when bundling your application for deployment (WAR).

NOTE: Some [previous attempt](http://johnnywey.wordpress.com/2010/03/02/grails-less-css-plugin/) had been made to integrate Less with Grails, but the solution required to pull out the full JRuby stack in order to work correctly. Now that Less has a javascript compiler, we are able to do exactly the same thing but with only Mozilla Rhino as a dependency. Rhino is a library to execute Javascript code directly in the JVM.

By using this new plug-in, you can now use Less transparently in your Grails projects. You first create a .less file in your `web-app/css` directory, then refer to it (without the file extension) in the HEAD section of your site using the available taglibs:

{{< highlight xml >}}
<less:stylesheet name="your_stylesheet" />
<less:stylesheet name="your_otherstylesheet" />
<less:scripts/>
{{< /highlight >}}

You may add any number of Less files to your page this way.

When in development mode, .less files will be parsed automatically and directly in the browser. Your changes will be reflected after each page reload.When building your project (using the `war` command), all your .less files will be compiled to standard CSS files, and the taglib will correctly refer to the static CSS files at runtime.

The plug-in should also play nicely with the [ui-performance](http://www.grails.org/plugin/ui-performance) plug-in: as long as the ui-performance plug-in executes after lesscss, generated CSS files can be minified and bundled by the plug-in.

See the [Github page](https://github.com/davidmarquis/grails-lesscss) for all the details on installing and using the plug-in.
