---
date:   2010-10-14T02:28:34Z
title:  Android emulator (virtual device) UnknownHostException on Mac OS X
slug:   android-emulator-virtual-device-unknownhostex
url:    /2010/10/14/android-emulator-virtual-device-unknownhostex/
tags:
 - android
---

Got an error while trying to run an application using the internet on an Android Virtual Device (AVD) on my Mac OS X Snow Leopard (1.6) computer : 

    java.net.UnknowHostException

Even the Android browser was not able to connect to the Internet. 

Solution? Turns out one must run the emulator with administrative privileges (using sudo...) in order to use the host computer's network connection!
