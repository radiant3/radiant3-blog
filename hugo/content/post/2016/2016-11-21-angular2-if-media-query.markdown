---
date:   2016-11-21T11:25:14Z
title:  'Angular 2: Structural directive for responsive conditional output using a CSS media query'
slug:   angular2-directive-responsive-conditional-output-using-css-media-query
tags:
 - frontend
 - angular2
---

Media queries are useful to adapt the way elements on a page are displayed, or even show/hide elements based on the viewport size.

When working with Angular 2, there might be times where you not only want to conditionally show or hide an element depending on the viewport size, but you also need to make sure that your component does not even get instantiated when the app is run on a mobile or desktop. Not creating components when they are not needed has obvious performance benefits!

## Enter the `ngIfMediaQuery` structural directive

At the end of this article is the code for a simple structural directive I wrote that will conditionally create a component based on the result of a CSS media query.

The directive can be used as such:

{{< highlight "html+ng2" >}}
<!-- div will only exist when the media query matches -->
<div *ngIfMediaQuery="'(min-width: 500px)'">
    ...
</div>
{{< /highlight >}}

This is conditionally creating a simple `div` element, but as with any other structural directive, it could be used on any component really.

{{% note title="TIP" type="success" %}}
Be sure to add the directive to the `declarations` section of your `NgModule` before using it in your application.
{{% /note %}}

Here's the [code](https://gist.github.com/davidmarquis/80e6d1ada3a024022f985a587b587825):

{{< gist davidmarquis 80e6d1ada3a024022f985a587b587825 >}}
