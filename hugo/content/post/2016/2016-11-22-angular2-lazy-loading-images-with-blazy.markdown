---
date:   2016-11-22T08:25:14Z
title:  'Angular 2: Lazy-loading images using bLazy.js'
slug:   angular2-lazy-load-images-with-blazy
tags:
 - frontend
 - angular2
---


I recently had the need to integrate [bLazy](http://dinbror.dk/blazy/) in an Angular 2 project. This is a pure Javascript library so the integration was straightforward but it does have a few drawbacks to be aware of, especially due to the way Angular renders elements.


The trick was wrapping bLazy initialization inside a `setTimeout()` which seems to be the only way for bLazy to properly set itself up.

Here's the [code](https://gist.github.com/davidmarquis/a702508f86679aa5a7b899a3499fbd15):

{{< gist davidmarquis a702508f86679aa5a7b899a3499fbd15 >}}

## How to use it?

There's an example in the directive documentation itself but here it is:

{{< highlight "html+ng2" >}}
<div id="blazyContainer" [bLazyLoadImages]="images" [bLazyOffset]="300" style="overflow: scroll;">
  <div *ngFor="let image of images">
    <img class="b-lazy" src="placeholder.jpg" [attr.data-src]="image" />
  </div>
</div>
{{< /highlight >}}

The sample assumes blazy.js is included in your application bundle. 

If you're using Webpack, only two steps are required:

  1. Install blazy with NPM: `npm install --save blazy`
  1. In your Typescript custom typings definitions file, add the line `declare module "blazy"`



