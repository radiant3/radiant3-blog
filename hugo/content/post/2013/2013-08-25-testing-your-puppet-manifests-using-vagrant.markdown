---
date:   2013-08-25T16:39:18Z
title:  Testing your Puppet manifests using Vagrant
slug:   testing-your-puppet-manifests-using-vagrant
url:    /2013/08/25/testing-your-puppet-manifests-using-vagrant/
tags:
- puppet
- devops
- vagrant
---

We've recently started using Puppet to provision our servers really seriously at work. All of our new servers are systematically provisioned using Puppet, and we now have a rule that nothing on servers can be changed by hand, every single bit needs to be managed by Puppet.

Managing servers using Puppet is extremely useful and powerful, but you also need to know what you're doing because a small untested change in your Puppet manifests could introduce instability (or worse!) in your production environment.

Quality is one of my top concern. In infrastructure management, quality is often measured from stability. Thus, I quickly felt the need to thoroughly test any change to our Puppet manifests in a completely isolated environment.

My search for the holy grail led me to the nirvana of Puppet testing: a combination of Vagrant and librarian-puppet.

## Vagrant

With [Vagrant](http://www.vagrantup.com/), you describe a virtual machine in a simple configuration file (a "Vagrantfile"), and then using the Vagrant command line tools, you can create, destroy or re-provision one or more virtual machines in a quick and repeatable manner. In the background, Vagrant uses the free Oracle Virtualbox for running the virtual machines (more recently, new "providers" have been added such as VMWare or even an experimental Amazon EC2 provider). You can also easily connect to your virtual server using SSH to validate what Puppet has done to your server. This is ideal for testing Puppet as you can scrap your VM whenever you need and restart from scratch in less time than it takes to make coffee.

## Librarian Puppet

[Librarian-puppet](https://github.com/rodjek/librarian-puppet) manages your Puppet module dependencies. In a file called `Puppetfile`, you describe which modules your infrastructure depends on, then librarian-puppet does the rest. It installs dependencies, upgrades them or removes them when needed. This is an invaluable tool to control when and how your module dependencies are updated.

## A quick kickstart project to get you running

If you want to try the workflow of managing a Puppet-provisioned virtual server in Vagrant, I've created a sample project that will help you jumpstart your setup. This project is [available on my Github account](https://github.com/davidmarquis/vagrant-puppet).

See the README file for details on what to install and how to run it. A few Puppet best practices are also described in that README file.

Let me know if you have any question! Enjoy!
