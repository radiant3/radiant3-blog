---
date:   2013-03-28T13:12:31Z
title:  'Tip: Adding a local HTTP proxy when using Yeoman 1.0 with Grunt and Livereload'
slug:   tip-adding-a-local-http-proxy-when-using-yeoman-1-0-with-grunt-and-livereload
url:    /2013/03/28/tip-adding-a-local-http-proxy-when-using-yeoman-1-0-with-grunt-and-livereload/
tags:
- angularjs
- grunt
- javascript
- frontend
---

{{% note title="UPDATE" type="success" %}}

Grunt and the plugins ecosystem is moving quite fast, and thus this post is a bit outdated now. Although the concepts still apply, Livereload is now [embedded in the "watch" plugin](https://github.com/gruntjs/grunt-contrib-watch#optionslivereload) and the way to [configure the "connect-proxy" plugin](https://github.com/drewzboto/grunt-connect-proxy#with-livereload) to achieve a local proxy to your Web app has changed quite a bit. Documentation has also improved so kudos to the Grunt community!

{{% /note %}}

[Yeoman](http://yeoman.io/) is a Javascript code generation and application bootstrapping framework that integrates many extremely cool, bleeding edge technologies for streamlining frontend rich Javascript application development.

We just started an [Angular.js](http://angularjs.org/) application at work and we used Yeoman to kickstart the application's structure. Not only did the initial project generation work (almost) flawlessly, but we discovered some truly amazing new tools like [Livereload](http://livereload.com/) (which really *is* the web developer wonderland!) and Compass.

Developing a rich client-side only Javascript with Yeoman's tooling requires the developer to run a simple local HTTP server to be able to see the application running. This HTTP server (Livereload) is out of this world: it will reload the app (including all code, SASS/CSS generation, images, etc.) on-the-fly upon any change in the source code. And by _reload_ I don't mean the usual _you refresh the page in your browser_, I mean _Livereload will refresh everything automatically as soon as you hit Save on any source file of your project_.

When we want to run our Angular.js application with Yeoman, all we do is run this command:

    
    grunt server


And we're off! The browser automatically launches on URL http://localhost:9000/ and displays our live-reloading application.

This is all extremely good stuff, but we quickly hit a road block. Since our application is being served by a local server that serves only static files, how do we interact with our backend server (a Django application in our case) given most browsers prevent cross-site scripting (XSS)?

More specifically, the local server running my Angular.js application is accessible on this address:


http://localhost:9000/


Whereas my Django app is running on this address:


http://localhost:8000/


If I try to do any remote Ajax call to my backend server application (the one that runs on port 8000), it will fail because my browser will refuse to run an XHR to a different origin. The solution most JS frameworks come with is to add a simple HTTP proxy feature within their local server to deal with the same-origin restrictions imposed by browsers.

Unfortunately, even if this use case is almost always encountered when developing a rich JS app locally, Yeoman's documentation on that use case is very scarce. So save yourself the search, here's the trick!


**Use the [grunt-connect-proxy module ](https://github.com/drewzboto/grunt-connect-proxy)and add a proxy to your gruntfile!**


Just diligently follow the procedure on the project's [GitHub page](https://github.com/drewzboto/grunt-connect-proxy) and it'll work. There is no mention anywhere that this works with Yeoman's Grunt setup, but it does!

After you install the module and apply the few changes to your project, your Grunt server will proxy requests to your backend application running locally. When running the "grunt server" command, you should see this:

    
    ...
    Running "configureProxies" task
    Proxy created for: /api
    ...


Which confirms your proxy is properly setup.
Just make sure your backend application's URLs are all prefixed by a specific path (ex: /api is often used) and you'll be good to go!
