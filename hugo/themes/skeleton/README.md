# Skeleton

Hugo theme based on the [Skeleton](http://www.getskeleton.com/) CSS toolkit.


## License

This theme is released under the [MIT license](LICENSE.md).